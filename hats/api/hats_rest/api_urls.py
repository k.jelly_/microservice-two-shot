from django.urls import path 
from .api_views import api_list_hats, api_delete_hat

urlpatterns = [
    path("locations/<int:location_vo_id>/hats/", api_list_hats, name="hats"),
    # path("locations/<int:location_vo_id>/hats/", api_list_hats, name="hats"),
    path("hats/", api_list_hats, name="list_hats"),
    path("locations/hats/", api_list_hats, name="hats"),
    path("hats/<int:pk>/", api_delete_hat, name="delete_hat"),

]
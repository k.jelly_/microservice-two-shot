

# Create your views here
import json
from django.http import JsonResponse
from .models import Hat, LocationVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["location_href", "closet_name"]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",

    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

# class HatListEncoder(ModelEncoder):
#         model = Hat
#         properties = ["name"]


def api_list_hats(request, location_vo_id=None):

    # return a dictionary with single key- "hats" which is
    # with a list of hat dictionaries that contain
    # The location is a property of the wardrobe and contains
    # style_name and link to detail for that hat
    # access the location using the warDrobeVo via polling

    # check if request is get or post
    if request.method == "GET":
        if location_vo_id is None:
            hats = Hat.objects.all()

            return JsonResponse(
                {"hats": hats},
                encoder=HatDetailEncoder)
        hats = Hat.objects.filter(location=location_vo_id)
        return JsonResponse(
            {"hats": hats}, encoder = HatDetailEncoder
        )

    else:
        content = json.loads(request.body)

        try:
            href = content["location"]
            location = LocationVO.objects.get(location_href = href)
            content["location"]=location

        except LocationVO.DoesNotExist: 
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat, encoder = HatDetailEncoder, safe = False
        )
@require_http_methods(["DELETE"])
def api_delete_hat(request, pk): 
    if request.method == "DELETE":
        count,_  = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count >0})
        
        

            

        # return JsonResponse({"message": "Not a get request or did not handle request properly"})

    # if is get
    # if conference id is none, get all hats objects

    # return JsonResponse( {"hats": hats})

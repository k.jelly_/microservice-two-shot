import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            style_name: '',
            color: '',
            picture_url: '', 
            location: '',
            locations: [],


        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handlecolorChange = this.handlecolorChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);

        
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }

    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({ style_name: value })
    }

    handlecolorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ picture_url: value })
    }

    handleLocationChange(event){
        const value = event.target.value;
        console.log(value)
        this.setState({ location: value })
    }

    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        console.log(data)
        delete data.locations;
        // console.log(data.location.location_href)

        const url = "http://localhost:8090/api/hats/";
        

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },

        };

        const response = await fetch(url, fetchConfig);
        if (response.ok){
            const newHat = await response.json();
            console.log(newHat)

        }
        
        
    }



    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)

            this.setState({ locations: data.locations});
            // console.log(this.state.locations)
        }
    }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Hat</h1>
                        <form onSubmit={this.handleSubmit} id ="create-hat-form">
                        {/* <form onSubmit={this.handleSubmit} id="create-location-form"> */}
                            <div className="form-floating mb-3">
                                <input value={this.state.fabric} onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                                <label htmlFor="name">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStyleChange} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control" />
                                <label htmlFor="room_count">Style name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlecolorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureChange} placeholder="picture_url" type="text" name="picture_url" id="picture_url" className="form-control"/>
                                <label htmlFor="picture">Picture url</label> 
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select" value={this.state.location}>
                                    <option value="">Choose a Location</option>
                                    {this.state.locations.map(location => {
                                        // console.log(location, "******")
                                        // console.log("location")

                                        return (

                                            <option key={location.href} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default HatForm;
function HatsList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Fabric</th>
          <th>Style</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {props.hats?.map((hat) => {
          return (
            <tr key={hat.closet_name}>
              <td>{hat.fabric}</td>
              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td>{hat.picture_url}</td>
              <td>{hat.location.closet_name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
import React from "react";

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: '',
            modelName: '',
            color: '',
            pictureUrl: '',
            shoeBin: '',
            bins: []
        }

        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModelNameChange = this.handleModelNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleShoeBinChange = this.handleShoeBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.model_name = data.modelName;
        data.picture_url =  data.pictureUrl;
        data.shoe_bin = data.shoeBin;
        delete data.modelName;
        delete data.pictureUrl;
        delete data.shoeBin;
        delete data.shoes;
        delete data.bins
        console.log(data)

        const shoeUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            const cleared = {
                manufacturer: '',
                modelName: '',
                color: '',
                pictureUrl: '',
                shoeBin: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/bins/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({bins: data.bins});
        }
    }



    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
    }

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({ modelName: value });
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({ pictureUrl: value });
    }

    handleShoeBinChange(event) {
        const value = event.target.value;
        this.setState({ shoeBin: value });
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={this.handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manfacturer</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleModelNameChange} value={this.state.modelName} placeholder="Model name" required type="text" id="model_name" className="form-control" />
                    <label htmlFor="model_name">Model name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" type="text" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePictureUrlChange} value={this.state.pictureUrl} placeholder="Picture Url" type="url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleShoeBinChange} value={this.state.shoeBin} required className="form-select" id="shoe_bin">
                      <option value="">Choose a bin</option>
                      {this.state.bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.href}>
                                {bin.closet_name}
                            </option>
                        )
                      })} 
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}
export default ShoeForm;
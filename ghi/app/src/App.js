import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import ShoesList from "./ShoesList";
import ShoeForm from "./ShoeForm";
import Nav from "./Nav";
import HatsList from "./HatList";
import HatForm from "./HatForm";
import HatDelete from "./HatDelete";

function App(props) {
  // if (props.hats === undefined) {
  //   return null
  // }
  // if (props.shoes === undefined) {
  //   return null;
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatsList hats={props.hats} />} />
          </Route>
          <Route paths="hats">
            <Route path="create" element={<HatForm />} />
          </Route>
          {/* <Route path="" element={<HatDelete hats={props.hats}/>}/> */}
        </Routes>
        <Routes>
          <Route path="shoes" element={<ShoesList shoes={props.shoes} />} />
        </Routes>
        <Routes>
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

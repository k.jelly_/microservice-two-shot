function ShoesList(props) {

    // function deleteShoes() = fetch(`http://localhost:8080/api/shoes/${id}`, {
    //     method: "DELETE",
    //     headers: {
    //         'Content-type': 'application/json'
    //     }
    // })

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manfacturer</th>
          <th>Model</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Bin</th>
        </tr>
      </thead>
      <tbody>
        {props.shoes.map((shoe) => {
          return (
            <tr key={shoe.closet_name}>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.picture_url}</td>
              <td>{shoe.shoe_bin.bin_number}</td>
              <td>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoesList;

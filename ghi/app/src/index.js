import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );


// reportWebVitals();
async function loadHats() {
  const response = await fetch('http://localhost:8090/api/hats/');
  if (response.ok){
    const data = await response.json(); 
    // console.log(data)
    root.render(
    <React.StrictMode> 
      <App hats={data.hats}/>
    </React.StrictMode>)
  } else {
    console.error(response)
  }
}
loadHats();
async function loadShoes() {
  const response = await fetch("http://localhost:8080/api/shoes/")
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App shoes={data.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadShoes();

// async function loadWardrobe() {
//   let hatData, shoeData;
//   const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
//   const hatResponse = await fetch('http://localhost:8090/api/hats/');

//   if (shoeResponse.ok) {
//     shoeData = await shoeResponse.json();
//     console.log('shoe data: ', shoeData)
//   } else {
//     console.error(shoeResponse);
//   }
//   if (hatResponse.ok) {
//     hatData = await hatResponse.json();
//     console.log('hat data: ', hatData)
//   } else {
//     console.error(hatResponse);
//   }

//   root.render(
//     <React.StrictMode>
//       <App shoes={shoeData} hats={hatData} />
//     </React.StrictMode>
//   );
// }
// loadWardrobe();
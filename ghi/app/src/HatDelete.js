function HatDelete(props) {

    console.log(props)

    function handleRemove(id) {
        console.log(id)
        // const newList = props.filter((item) => item.id !== id);
        // props = newList
        // const url = 




    }

    return (

        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>

                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    console.log(hat)
                    return (
                        <tr>
                            <td>{hat.fabric}</td>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td>{hat.picture_url}</td>
                            <td>{hat.location.closet_name}</td>
                            <td>
                                <button type="button" onClick={() => handleRemove(hat.id)}>
                                    Remove
                                </button>
                            </td>
                        </tr>

                    );
                })
                }

            </tbody>
        </table>

    )
}

export default HatDelete 